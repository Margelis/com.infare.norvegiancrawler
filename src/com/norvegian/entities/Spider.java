package com.norvegian.entities;

import java.time.LocalDateTime;

public class Spider {
	private LocalDateTime depTime;
	private String depAirport;
	private LocalDateTime arrTime;
	private String arrAirport;
	private String connAirport;
	private Double cheapestPrice;

	public Spider(LocalDateTime depTime, LocalDateTime arrTime,
			Double cheapestPrice) {
		this.depTime = depTime;
		this.arrTime = arrTime;
		this.cheapestPrice = cheapestPrice;
	}

	public Spider() {
	};

	public String getDepAirport() {
		return depAirport;
	}

	public void setDepAirport(String depAirport) {
		this.depAirport = depAirport;
	}

	public String getArrAirport() {
		return arrAirport;
	}

	public void setArrAirport(String arrAirport) {
		this.arrAirport = arrAirport;
	}

	public String getConnAirport() {
		return connAirport;
	}

	public void setConnAirport(String connAirport) {
		this.connAirport = connAirport;
	}

	public Double getCheapestPrice() {
		return cheapestPrice;
	}

	public LocalDateTime getDepTime() {
		return depTime;
	}

	public void setDepTime(LocalDateTime depTime) {
		this.depTime = depTime;
	}

	public LocalDateTime getArrTime() {
		return arrTime;
	}

	public void setArrTime(LocalDateTime arrTime) {
		this.arrTime = arrTime;
	}

	public void setCheapestPrice(Double cheapestPrice) {
		this.cheapestPrice = cheapestPrice;
	}

	@Override
	public String toString() {
		return "Spider [DepartureTime=" + depTime + ", DepartureAirport=" + depAirport
				+ ", ArrivalTime=" + arrTime + ", ArrivalAirport=" + arrAirport
				+ ", ConnectionAirport=" + connAirport + ", CheapestPrice="
				+ cheapestPrice + "]";
	}

}
