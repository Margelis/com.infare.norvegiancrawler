package com.norvegian.controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.norvegian.entities.Spider;

public class SpiderController {
	// OSL - RIG, direct.
	public static String urlTemplate = "http://www.norwegian.com/uk/booking/flight-tickets/select-flight/?D_City=OSL&A_City=RIX&TripType=1&D_Day=%s&D_Month=%s&D_SelectedDay=%s&R_Day=01&R_Month=201612&R_SelectedDay=01&IncludeTransit=false&AgreementCodeFK=-1&CurrencyCode=GBP&rnd=92173&processid=14185";

	// OSL - RIG, all flights.
	//public static String urlTemplate = "http://www.norwegian.com/uk/booking/flight-tickets/select-flight/?D_City=OSL&A_City=RIX&TripType=1&D_Day=%s&D_Month=%s&D_SelectedDay=%s&R_Day=02&R_Month=201612&R_SelectedDay=02&AgreementCodeFK=-1&CurrencyCode=GBP&ShowNoFlights=True&rnd=17173";
	
	/**
	 * Finds all flights data in given date range.
	 * @param departingStartDatePattern - start date pattern (yyyyMMdd), ex.: 20161201
	 * @param departingEndDatePattern - end date pattern (yyyyMMdd), ex.: 20161231
	 * @return returns all flights in given range.
	 */
	public List<Spider> findFlightsInRangeFor(String departingStartDatePattern,
			String departingEndDatePattern) {
		
		LocalDate startDate = getLocalDateFrom(generateLocalDateTimeStringFor(
				departingStartDatePattern.substring(0, 6),
				departingStartDatePattern.substring(6, 8)));
		LocalDate endDate = getLocalDateFrom(generateLocalDateTimeStringFor(
				departingEndDatePattern.substring(0, 6),
				departingEndDatePattern.substring(6, 8)));
		List<Spider> flightsList = new ArrayList<>();

		for (LocalDate date = startDate; date.isBefore(endDate.plusDays(1)); date = date
				.plusDays(1)) {
			flightsList
					.addAll(findFlightsForGivenDate(
							date.getYear() + "" + date.getMonthValue(),
							String.valueOf(String.format("%02d",
									date.getDayOfMonth()))));
		}

		return flightsList;
	}
	
	/**
	 * Finds all flights for given date.
	 * @param monthPattern - year and month pattern (yyyyMM), ex.: 201613.
	 * @param dayPattern - day pattern (dd), ex.: 01.
	 * @return return all flights for given date.
	 */
	public List<Spider> findFlightsForGivenDate(String monthPattern,
			String dayPattern) {
		Document doc = null;
		try {
			doc = Jsoup.connect(
					String.format(urlTemplate, dayPattern, monthPattern,
							dayPattern)).timeout(10*1000).get();
		} catch (IOException e) {
			System.err.println("An error occurs when connecting to source: "
					+ e.getMessage());
		}

		Elements tableElement = doc
				.select(".sectionboxavaday .bodybox .body .avadaytable tbody");

		return findFlightsDataFor(tableElement, monthPattern, dayPattern);
	}

	private List<Spider> findFlightsDataFor(Elements tableElement,
			String monthPattern, String dayPattern) {
		List<Spider> spidersList = new ArrayList<Spider>();
		Spider currentSpider = null;
		int counter = 0;

		for (Element element : tableElement.select("tbody tr")) {
			if (counter == 0) {
				currentSpider = createNewSpider(monthPattern, dayPattern,
						element);
				counter++;
			} else if (counter == 1) {
				setDepartureAndArrivalAirports(currentSpider, element);
				counter++;
			} else if (counter == 2) {
				setAirportConnections(currentSpider, element);
				spidersList.add(currentSpider);
				currentSpider = null;
				counter = 0;
			}
		}
		return spidersList;
	}

	private void setAirportConnections(Spider currentSpider, Element element) {
		String connAirport = element
				.select(".lastrow .routeinfotextcell .content .flightinfolist .TooltipBoxTransit")
				.text();
		currentSpider.setConnAirport(connAirport);
	}

	private void setDepartureAndArrivalAirports(Spider currentSpider,
			Element element) {
		String depAirport = element.select(".rowinfo2 .depdest .content")
				.text();
		String arrAirport = element.select(".rowinfo2 .arrdest .content")
				.text();
		currentSpider.setDepAirport(depAirport);
		currentSpider.setArrAirport(arrAirport);
	}

	private Spider createNewSpider(String monthPattern, String dayPattern,
			Element element) {

		String depTime = element.select(".rowinfo1 .depdest .content").text();
		String arrTime = element.select(".rowinfo1 .arrdest .content").text();
		Double cheapestPrice = Double.valueOf(element.select(
				".rowinfo1 .standardlowfare .content").text());
		String localDepTime = generateLocalDateTimeStringFor(monthPattern,
				dayPattern) + " " + depTime;

		String localArrTime = generateLocalDateTimeStringFor(monthPattern,
				dayPattern) + " " + arrTime;
		return new Spider(getLocalDateTimeFrom(localDepTime),
				getLocalDateTimeFrom(localArrTime), cheapestPrice);
	}

	private LocalDateTime getLocalDateTimeFrom(String dateValue) {// 2016-12-01 12:10
		DateTimeFormatter formatter = DateTimeFormatter
				.ofPattern("yyyy-MM-dd HH:mm");
		LocalDateTime date = LocalDateTime.parse(dateValue, formatter);
		return date;
	}

	private LocalDate getLocalDateFrom(String dateValue) {// 2016-12-01
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate date = LocalDate.parse(dateValue, formatter);

		return date;
	}

	private String generateLocalDateTimeStringFor(String monthPattern,
			String dayPattern) {
		String year = monthPattern.substring(0, 4);
		String month = monthPattern.substring(4, 6);
		return year + "-" + month + "-" + dayPattern;
	}
}
