package com.norvegian;

import java.util.List;

import com.norvegian.controllers.SpiderController;
import com.norvegian.entities.Spider;

public class Main {

	public static void main(String[] args) {
		
		SpiderController controler = new SpiderController();
		print(controler.findFlightsInRangeFor("20161201", "20161231"));
	}
	
	public static void print(List<Spider> spiderList){
		
		for(Spider spider : spiderList){
			System.out.println(spider.toString());
		}
		
	}

}
